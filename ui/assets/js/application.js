
var stream = [{key: 'total', values: []}, {key: 'success', values: []}];

function test_me(to_go){
    if (!to_go)
	to_go = 13;
    nv.addGraph(function() {
	nv.graphs = [];
	var chart = nv.models.lineWithFocusChart();
	chart.xAxis
	    .tickFormat(d3.format(',f'));
	chart.x2Axis
	    .tickFormat(d3.format(',f'));
	
	chart.yAxis
	    .tickFormat(d3.format(',.2f'));
	chart.y2Axis
	    .tickFormat(d3.format(',.2f'));
	
	d3.select('#chart svg')
	//.datum(testData())
	    .datum(stream)
	    .transition().duration(0)
	    .call(chart);
	
	nv.utils.windowResize(chart.update);
	
	return chart;
}); 


    function testData() {
	return stream_layers(2,to_go,.1).map(function(data, i) {
	    return { 
		key: 'Stream' + i,
		values: data
	    };
	});
    }
    
};



var socket = io.connect('http://localhost:3000');

var last_total = 0;
var last_success = 0;

socket.on('connect', function () { 
    socket.on('stream', function(obj){
	
	console.log (obj)
	var percentage = ((((obj.success - last_success) * 100) / (obj.total - last_total)));
	console.log(percentage);
	console.log(obj.success - last_success);
	if (!isNaN(percentage)){
	    stream[0].values.push({y: percentage, x: stream[0].values.length})
	stream[1].values.push({y: 100, x: stream[1].values.length})
	
	last_total = obj.total;
	last_success = obj.success;
	}
	test_me();
    });
    
    
    $('#start-training').click(function(){
	socket.emit('start_training');
    });
    $('#stop-training').click(function(){
	socket.emit('stop_training');
	last_total = 0;
	last_success = 0;
    });
});