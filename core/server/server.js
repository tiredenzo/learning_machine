var redis = require("redis"),
redis_publish = redis.createClient(),
redis_subscribe = redis.createClient()

var total = 0;
var success = 0;


var io = require('socket.io').listen(3000);
var ai = []

io.sockets.on('connection', function (socket) {
    socket.on('stop_training', function(){
	total = 0;
	success = 0;
	redis_publish.publish("node_to_ruby", "stop_training");
    });
    socket.on('start_training', function(){
	redis_publish.publish("node_to_ruby", "start_training");
    });

    (function stream(){
	//if ((total % 60) == 0)
	  
	setTimeout(stream, 4000)
    })();
});




redis_subscribe.subscribe("ruby_to_node", function(){
    redis_subscribe.on("message", function(message, m){
	total += 1
	success += JSON.parse(m)
	if (total % 60 == 0)
	    io.sockets.emit('stream',{total: total, success: success})
    });
});