require 'json'
require 'redis'
require './ai'

class Oracle
  # EPITECH, EPITA, MIT, SUPINTERNET, SUPINFO, HEC, 
    def discretise  x, precision, range 
      x = x.to_i
      raise 'number to discretise is not in range' unless range.include? x
      x_save = x
      x = (x / precision) * precision
      x += precision if (x + precision / 2.0 < x_save )
      x.to_s
    end  

  def reset
        @data = {
      labels: [ {attribute: 'math', apply: -> x { discretise(x, 5, 0..20) } },
                {attribute: 'physic', apply: -> x { discretise(x, 5, 0..20) } },
                {attribute: 'computing', apply: -> x { discretise(x, 5, 0..20) } },
                {attribute: 'economy', apply: -> x { discretise(x, 5, 0..20) } },
                {attribute: 'management', apply: -> x { discretise(x, 5, 0..20) } },
                {attribute: 'volatility' },
                {attribute: 'wants_to_make_money'},
                {attribute: 'is_autist'},
                {class: 'shcool'}], # EPITECH, EPITA, ENS, SUPINTERNET, SUPINFO, HEC, MIT
                items: []
              }
  end

  def train    
    epitech = -> instance {
      ret = 0
      ret += 1 if instance[:math].to_i <= 10
      ret += 1 if instance[:physic].to_i <= 10
      ret += 1 if instance[:computing].to_i >= 10
      ret += 1 if instance[:economy].to_i <= 5
      ret += 1 if instance[:management].to_i <= 10
      ret += 1 if instance[:volatility].to_i == 1
      ret += 1 if instance[:wants_to_make_money].to_i == 0
      ret += 1 if instance[:is_autist].to_i == 1

      ret
    }

=begin
    epita = -> instance {
      ret = 0
      ret += 2 if instance[:math].to_i >= 10
      ret += 2 if instance[:computing].to_i >= 10
      ret += 2 if instance[:physic].to_i >= 10
      ret += 2 if instance[:is_autist].to_i == 0
      ret
    }

    mit = -> instance {
      ret = 0
      ret += 2 if instance[:math].to_i >= 15
      ret += 2 if instance[:computing].to_i >= 15
      ret += 3 if instance[:physic].to_i >= 15
      ret += 1 if instance[:is_autist].to_i == 1
      ret
    }
=end
    hec = -> instance {
      ret = 0
      ret += 1 if instance[:math].to_i >= 10
      ret += 1 if instance[:physic].to_i >= 10
      ret += 1 if instance[:computing].to_i < 10
      ret += 1 if instance[:economy].to_i >= 15
      ret += 1 if instance[:management].to_i >= 15
      ret += 1 if instance[:volatility].to_i == 0
      ret += 1 if instance[:wants_to_make_money].to_i == 1
      ret += 1 if instance[:is_autist].to_i == 0
      ret
    }

    supinfo = -> instance {
      ret = 0
      ret += 1 if instance[:math].to_i <= 5
      ret += 1 if instance[:physic].to_i <= 5
      ret += 1 if instance[:computing].to_i <= 5
      ret += 1 if instance[:economy].to_i <= 5
      ret += 1 if instance[:management].to_i <= 5
      ret += 1 if instance[:volatility].to_i == 1
      ret += 1 if instance[:wants_to_make_money].to_i == 0
      ret += 1 if instance[:is_autist].to_i == 0
      ret
    }

    
    awaited_result = -> instance {
      k_save = nil
      v_save = nil
      {
        supinfo: supinfo.call(instance),
        epitech: epitech.call(instance),
        #epita: epita.call(instance),
        hec: hec.call(instance)
        #mit: mit.call(instance)
      }.each { |k, v|
        if v_save.nil? or v > v_save
          k_save = k
          v_save = v
        end
      }
      k_save.to_s
    }


    reset
    redis_publish = Redis.new driver: :hiredis
    id3 = MyID3.new @data
    l = id3.gen_code
    
    while  @train_order
      begin
        instance = {math: discretise(rand(0..20), 5, 0..20),
          physic: discretise(rand(0..20), 5, 0..20),
          computing: discretise(rand(0..20), 5, 0..20),
          economy: discretise(rand(0..20), 5, 0..20),
          management: discretise(rand(0..20), 5, 0..20),
          volatility: rand(0..1).to_s,
          wants_to_make_money: rand(0..1).to_s,
          is_autist: rand(0..1).to_s}

        awaited = awaited_result.call(instance)
        ans = nil
        ans = l.call(instance)
        
        if ans == awaited
          puts 'good learning ' + awaited
          redis_publish.publish 'ruby_to_node', 1.to_json
        else
          redis_publish.publish 'ruby_to_node', 0.to_json
          puts "bad learning should be  #{awaited} is #{ans}"
          raise ID3NotEnoughLearningError
        end
      rescue ID3NotEnoughLearningError
        to_put = []
        @data[:labels].each_with_index do |label, index| 
          next if index == @data[:labels].size - 1
          to_put << instance[label[:attribute].to_sym].to_s
        end
        to_put << awaited_result.call(instance)
        @data[:items] << to_put unless  @data[:items].include? to_put
        id3 = MyID3.new @data
        l = id3.gen_code
      rescue => e
        raise "Something went wrong in ID3... #{e.message}"
      end
    end
  end


  def mdr
    puts 42
  end

  def get_redis_order
    train_thread = nil
    redis_subscribe = Redis.new driver: :hiredis
    redis_subscribe.subscribe('node_to_ruby') do |on|
      on.message do |channel, msg|
        case msg
        when "start_training"
          @train_order = true
          train_thread = Thread.new { train } if train_thread.nil?
        when "stop_training"
          @train_order = false
          train_thread.join unless train_thread.nil?
          train_thread = nil
        else
        end
      end
    end
  end
end

Oracle.new.get_redis_order

