# -*- coding: utf-8 -*-

class ID3NotEnoughLearningError < Exception; end

class MyID3
  attr_accessor :tree
  attr_accessor :string
  def initialize _data
    @data = _data

    # check arguments and boot
    god_proc = Proc.new { |data|
     
      match_attributes = -> index {
        attribute = {}
        data[:items].each do |i|
          attribute[i[index]] = 0 if attribute[i[index]].nil?
          attribute[i[index]] += 1
        end
        attribute
      }
      
      match_attributes_on_classes = -> index {
        attributes = {}
        lambda_call = -> i {
          if data[:labels][index][:apply].nil?
            i[index]
          else
            data[:labels][index][:apply].call(i[index])
          end
        }
        data[:items].each do |i|
          attributes[i[index]] = {} if attributes[i[index]].nil?
          attributes[i[index]][i.last] = 0 if attributes[i[index]][i.last].nil?
          attributes[i[index]][i.last] += 1  
        end
        attributes
      }
      
      define_entropy = -> attribute { 
        total = Float(0)
        attribute = attribute.map { |k,v| v }
        size_attr  = (attribute.inject{|sum,x| sum + x }).to_f
        attribute.each { |cl_count|
          total += cl_count / size_attr * Math.log(cl_count.to_f / size_attr, 2)
        }
        total *= - 1
      }

      gain_attributes = -> {
        entropy = define_entropy.call(match_attributes.call(data[:labels].size - 1))
        attributes = {}
        data[:labels].each_with_index { |attribute, i_attribute| 
          next if i_attribute == data[:labels].size - 1
          values = match_attributes_on_classes.call(i_attribute)
          values_count = match_attributes.call(i_attribute)
          total = Float(0)
          values.each { |k,v|
            total += (values_count[k].to_f / data[:items].size) * define_entropy.call(v)
            values[k] = nil
          }
          attributes[attribute[:attribute]] = {gain: entropy - total , values: values, i_attribute: i_attribute}
        }
        attributes
      }
      
      #build tree
      (build_tree = -> {
         node = nil
         gain_attributes.call.each { |k, v| node = {k => v} if node.nil? or node.first.last[:gain] < v[:gain] }
         
         raise ({attr: gain_attributes.call(), data: data }.inspect + 'Oracle Learning error ;)') if node.nil?
         
         i_attribute = node.first.last[:i_attribute]
         
         values = node.first.last[:values]
         values.each { |v, c|

           new_data = Marshal.load(Marshal.dump(data))
           new_data[:labels].delete_at i_attribute
           new_data[:items].map! { |i|
             next if i[i_attribute] != v
             i.delete_at i_attribute
             i
           }
           new_data[:items].delete_if {|x| x == nil}
           class_cmp = -> { 
             cmp =  new_data[:items].first.last  
             new_data[:items].each { |i| 
               return false if i.last != cmp
             }
             true
           }
           if  !class_cmp.call
             # begin
             values[v] = god_proc.call(new_data)
             # rescue
             #  throw data
             #end
           else 
             values[v] = new_data[:items].first.last
           end
         }
           @tree = node
           node
       }).call
    }
    
         (-> {
            @data[:items].each_with_index { |i, index|
              raise "Argument length error in labels line #{index + 1}" unless i.size ==  @data[:labels].size
              i.each_with_index { |v, index_1|
                raise "Value is not a String [#{index}][#{index_1}] attach a lambda to the label to have fun " unless v.class == String
              }
            }

            label_save = @data[:labels].clone

            label_save.map! do |e|
              e.clone
            end

            @data[:labels].each_with_index do |l, l_index| 
              unless l[:apply].nil? 
                @data[:items].map do |i| 
                  i[l_index] = @data[:labels][l_index][:apply].call(i[l_index])
                  raise "Lambda didn't return a string" if i[l_index].class != String
                  i
                end
                l[:apply] = nil
              end
            end
            god_proc.call @data
            @data[:labels] = label_save
          }).call
       end

       def gen_code
         string = ''
         (loop = -> tree, level {
            tree.each {|t_k, t_v|
              t_v[:values].each_with_index { |v, v_i|
                v_k = v[0];  v_v = v[1]
                level.times { string << ' '}
                string << "#{v_i > 0 ? 'els' : ''}if instance[:#{t_k}] == '#{v_k}'\n"
                if v_v.class == Hash
                  loop.call(v_v, level + 1)
                else
                  level.times { string << ' '}
                  string << " return '#{v_v}'\n"
                end
                
                if v_i == t_v[:values].size - 1
                  level.times { string << ' '}
                  string << "end\n"
                  if level == 0
                    string << "raise ::ID3NotEnoughLearningError, 'not enough learning'\n"
                  end
                end
              }
            }
          }).call(@tree, 0)
         string << "raise ::ID3NotEnoughLearningError, 'not enough learning'\n" if string.size == 0
         @string = string
         eval "-> instance { #{string} }"
       end
end

=begin

data = {
       labels: [ {attribute: 'sky', apply: -> x { x } },{attribute: 'temperature'},{attribute: 'humidity'},{attribute: 'wind'},{attribute: 'class'}],
       items: [
               ['sunny', 'hot', 'high', 'low', 'F'],
               ['sunny', 'hot', 'high', 'strong', 'F'],
               ['cloudy', 'hot', 'high', 'low', 'T'],
               ['rainy', 'cool', 'high', 'low', 'T'],
               ['rainy', 'cold', 'normal', 'low', 'T'],
               ['rainy', 'cold', 'normal', 'strong', 'F'],
               ['cloudy', 'cold', 'normal', 'strong', 'T'],
               ['sunny', 'cool', 'high', 'low', 'F'],
               ['sunny', 'cold', 'normal','low', 'T'],
               ['rainy', 'cool', 'normal', 'low', 'T'],
               ['sunny', 'cool', 'normal', 'strong', 'T'],
               ['cloudy', 'cool', 'high','strong', 'T'],
               ['cloudy', 'hot', 'normal', 'low', 'T'],
               ['rainy', 'cool', 'high','strong', 'F']
              ]
       
     }
     require 'pp'
     id3 = MyID3.new data
     l = id3.gen_code
     pp id3.tree
=end
